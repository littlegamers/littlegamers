﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour{

    [SerializeField]
    private AudioClip mainMenuTheme = null;
    [SerializeField]
    private AudioClip mapTheme = null;
    [SerializeField]
    private AudioClip eventTheme = null;
    [SerializeField]
    private AudioClip battleTheme = null;
    [SerializeField]
    private AudioClip[] match_blocks = null;
    [SerializeField]
    private AudioClip enemy_armor_up = null;
    [SerializeField]
    private AudioClip enemy_heal = null;
    [SerializeField]
    private AudioClip enemy_magic_armor = null;
    [SerializeField]
    private AudioClip enemy_magic_attack = null;
    [SerializeField]
    private AudioClip enemy_physical_attack = null;
    [SerializeField]
    private AudioClip player_physical_attack = null;
    [SerializeField]
    private AudioClip player_magic_attack = null;
    [SerializeField]
    private AudioClip player_hurt = null;
    [SerializeField]
    private AudioClip cultists_dead = null;
    [SerializeField]
    private AudioClip cultists_hurt = null;
    [SerializeField]
    private AudioClip minotaur_dead = null;
    [SerializeField]
    private AudioClip[] minotaur_hurt = null;
    [SerializeField]
    private AudioClip skeleton_dead = null;
    [SerializeField]
    private AudioClip skeleton_hurt = null;
    [SerializeField]
    private AudioClip failure = null;
    [SerializeField]
    private AudioClip victory = null;
    [SerializeField]
    private AudioClip buy = null;


    [SerializeField]
    private AudioSource blockEffectSource = null;
    [SerializeField]
    private AudioSource enemyEffectSource = null;
    [SerializeField]
    private AudioSource playerEffectSource = null;
    [SerializeField]
    private AudioSource voEffectSource = null;
    [SerializeField]
    private AudioSource effectSource = null;

    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start(){
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;
    }

    public void PlayMusic(string track) {
        audioSource.Stop();

        switch (track) {
            case "mainMenu":
                audioSource.clip = mainMenuTheme;
                break;
            case "map":
                audioSource.clip = mapTheme;
                break;
            case "event":
                audioSource.clip = eventTheme;
                break;
            case "battle":
                audioSource.clip = battleTheme;
                break;
        }

        audioSource.Play();
    }

    public void PlaySoundEffect(string effect) {

        switch (effect) {
            case "match":
                blockEffectSource.Stop();
                blockEffectSource.clip = match_blocks[Random.Range(0, match_blocks.Length)];
                blockEffectSource.Play();
                break;
            case "enemyArmor":
                enemyEffectSource.Stop();
                enemyEffectSource.clip = enemy_armor_up;
                enemyEffectSource.Play();
                break;
            case "enemyMArmor":
                enemyEffectSource.Stop();
                enemyEffectSource.clip = enemy_magic_armor;
                enemyEffectSource.Play();
                break;
            case "enemyHeal":
                enemyEffectSource.Stop();
                enemyEffectSource.clip = enemy_heal;
                enemyEffectSource.Play();
                break;
            case "enemyMagicAttack":
                enemyEffectSource.Stop();
                enemyEffectSource.clip = enemy_magic_attack;
                enemyEffectSource.Play();
                break;
            case "enemyPhysicalAttack":
                enemyEffectSource.Stop();
                enemyEffectSource.clip = enemy_physical_attack;
                enemyEffectSource.Play();
                break;
            case "playerPhysicalAttack":
                playerEffectSource.Stop();
                playerEffectSource.clip = player_physical_attack;
                playerEffectSource.Play();
                break;
            case "playerMagicAttack":
                playerEffectSource.Stop();
                playerEffectSource.clip = player_magic_attack;
                playerEffectSource.Play();
                break;
            case "playerHurt":
                voEffectSource.Stop();
                voEffectSource.clip = player_hurt;
                voEffectSource.Play();
                break;
            case "cultistHurt":
                voEffectSource.Stop();
                voEffectSource.clip = cultists_hurt;
                voEffectSource.Play();
                break;
            case "cultistDead":
                voEffectSource.Stop();
                voEffectSource.clip = cultists_dead;
                voEffectSource.Play();
                break;
            case "skeletonHurt":
                voEffectSource.Stop();
                voEffectSource.clip = skeleton_hurt;
                voEffectSource.Play();
                break;
            case "skeletonDead":
                voEffectSource.Stop();
                voEffectSource.clip = skeleton_dead;
                voEffectSource.Play();
                break;
            case "minotaurHurt":
                voEffectSource.Stop();
                voEffectSource.clip = minotaur_hurt[Random.Range(0, minotaur_hurt.Length)];
                voEffectSource.Play();
                break;
            case "minotaurDead":
                voEffectSource.Stop();
                voEffectSource.clip = minotaur_dead;
                voEffectSource.Play();
                break;
            case "failure":
                effectSource.Stop();
                effectSource.clip = failure;
                effectSource.Play();
                break;
            case "victory":
                effectSource.Stop();
                effectSource.clip = victory;
                effectSource.Play();
                break;
            case "buy":
                effectSource.Stop();
                effectSource.clip = buy;
                effectSource.Play();
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
