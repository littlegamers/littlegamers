﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour{

    [SerializeField]
    private MapController mapController = null;
    [SerializeField]
    private GameObject mapCanvas = null;
    [SerializeField]
    private PuzzleController puzzleController = null;
    [SerializeField]
    private GameObject puzzleCanvas = null;
    [SerializeField]
    private Shop shopController = null;
    [SerializeField]
    private GameObject shopCanvas = null;

    // Start is called before the first frame update
    void Start(){
        puzzleCanvas.SetActive(false);
        shopCanvas.SetActive(false);
    }

    public void StartEvent(Event node_event) {
        if (node_event.type == 0) {
            StartPuzzle(node_event);
        } else if(node_event.type == 2) {
            StartShop();
        }
    }

    public void StartPuzzle(Event node_event) {
        puzzleCanvas.SetActive(true);
        puzzleController.enabled = true;
        puzzleController.StartPuzzle(node_event);
        mapCanvas.SetActive(false);
        mapController.enabled = false;
    }

    public void FinishPuzzle() {
        mapCanvas.SetActive(true);
        mapController.enabled = true;
        mapController.UpdateMap();
        puzzleCanvas.SetActive(false);
        puzzleController.enabled = false;
    }

    public void StartShop() {
        shopCanvas.SetActive(true);
        shopController.enabled = true;
        shopController.StartShop();
        mapCanvas.SetActive(false);
        mapController.enabled = false;
    }

    public void FinishShop() {
        mapCanvas.SetActive(true);
        mapController.enabled = true;
        mapController.UpdateMap();
        shopCanvas.SetActive(false);
        shopController.enabled = false;
    }

}

public struct Event {
    public int type;
    public Enemy enemy;
}
