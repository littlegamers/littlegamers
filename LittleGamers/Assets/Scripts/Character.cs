﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public class Character : MonoBehaviour{

    private int hp;
    private int maxHP;
    private int gold;
    private int shieldMultiplier;
    private int attackMultiplier;
    private int moves;
    private List<string> items;


    void Start(){
        hp = 40;
        maxHP = 40;
        gold = 0;
        items = new List<string>();
        shieldMultiplier = 1;
        attackMultiplier = 1;
        moves = 3;
    }

    public int getHP() {
        return hp;
    }

    public void setHP(int hp) {
        this.hp = hp;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public void setMaxHP(int hp) {
        this.maxHP = hp;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getAttackMultiplier() {
        return attackMultiplier;
    }

    public void setAttackMultiplier(int attackMultiplier) {
        this.attackMultiplier = attackMultiplier;
    }

    public int getShieldMultiplier() {
        return shieldMultiplier;
    }

    public void setShieldMultiplier(int shieldMultiplier) {
        this.shieldMultiplier = shieldMultiplier;
    }

    public int getMoves() {
        return moves;
    }

    public void setMoves(int moves) {
        this.moves = moves;
    }

    public List<string> getItems() {
        return items;
    }

    public void addItem(string item) {
        items.Add(item);
    }

    public void removeItem(string item) {
        items.Remove(item);
    }
}
