﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PuzzleRune : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {

    public enum Type { Sword, Wand, Shield, MShield, Heal, Dodge }

    public int x;
    public int y;
    private Type type;
    private PuzzleController puzzleController;

    private bool touched;
    private int pointerID;
    private Vector2 mouseOrigin;

    void Awake() {
        touched = false;
    }

    public void SetRuneAttributes(int x, int y, Type type, PuzzleController puzzleController) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.puzzleController = puzzleController;

        GetComponent<Animator>().SetInteger("Type", (int)type);
    }

    public void SetXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Type GetRuneType() {
        return type;
    }

    public void OnPointerDown(PointerEventData data) {
        if (!touched) {
            touched = true;
            pointerID = data.pointerId;
            mouseOrigin = data.position;
        }
    }

    public void OnDrag(PointerEventData data) {
        if (data.pointerId == pointerID) {
            Vector2 currentPosition = data.position;
            Vector2 directionRaw = currentPosition - mouseOrigin;
            puzzleController.PotentialMove(x, y, directionRaw);

        }
    }

    public void OnPointerUp(PointerEventData data) {
        if (data.pointerId == pointerID) {
            Vector2 currentPosition = data.position;
            Vector2 directionRaw = currentPosition - mouseOrigin;
            Vector2 swapPosition = puzzleController.PotentialMove(x, y, directionRaw);
            puzzleController.MakeMove(x, y, swapPosition);
            touched = false;
        }
    }
}
