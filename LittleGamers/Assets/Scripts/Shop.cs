﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [SerializeField]
    private GameObject mainScreen = null;
    [SerializeField]
    private GameObject upgradeScreen = null;
    [SerializeField]
    private Text fullHealText = null;
    [SerializeField]
    private Button fullHealButton = null;
    [SerializeField]
    private Text playerHealthText = null;
    [SerializeField]
    private Text playerCoinText = null;
    [SerializeField]
    private Text healthUpgradeText = null;
    [SerializeField]
    private Button healthUpgradeButton = null;
    [SerializeField]
    private Text attackUpgradeText = null;
    [SerializeField]
    private Button attackUpgradeButton = null;
    [SerializeField]
    private Text defenseUpgradeText = null;
    [SerializeField]
    private Button defenseUpgradeButton = null;
    [SerializeField]
    private Text moveUpgradeText = null;
    [SerializeField]
    private Button moveUpgradeButton = null;

    private int fullHealCost;
    private Character character;


    public void StartShop() {
        FindObjectOfType<SoundManager>().PlayMusic("event");

        character = FindObjectOfType<Character>();

        UpdateUI();
    }

    public void BuyHeal() {
        FindObjectOfType<SoundManager>().PlaySoundEffect("buy");
        character.setHP(character.getMaxHP());
        character.setGold(character.getGold() - fullHealCost);
        UpdateUI();
    }

    public void UpgradeScreen() {
        upgradeScreen.SetActive(true);
        mainScreen.SetActive(false);
    }

    public void MainScreen() {
        mainScreen.SetActive(true);
        upgradeScreen.SetActive(false);
    }

    public void BuyHealth() {
        FindObjectOfType<SoundManager>().PlaySoundEffect("buy");
        character.setMaxHP(character.getMaxHP() + 10);
        character.setHP(character.getMaxHP());
        character.setGold(character.getGold() - 5);
        UpdateUI();
    }

    public void BuyAttack() {
        FindObjectOfType<SoundManager>().PlaySoundEffect("buy");
        character.setAttackMultiplier(character.getAttackMultiplier() + 1);
        character.setGold(character.getGold() - 10);
        UpdateUI();
    }

    public void BuyDefense() {
        FindObjectOfType<SoundManager>().PlaySoundEffect("buy");
        character.setShieldMultiplier(character.getShieldMultiplier() + 1);
        character.setGold(character.getGold() - 5);
        UpdateUI();
    }

    public void BuyMoves() {
        FindObjectOfType<SoundManager>().PlaySoundEffect("buy");
        character.setMoves(character.getMoves() + 1);
        character.setGold(character.getGold() - 10);
        UpdateUI();
    }

    public void Back() {
        FindObjectOfType<EventManager>().FinishShop();
    }

    private void UpdateUI() {
        playerHealthText.text = "" + character.getHP();
        playerCoinText.text = "" + character.getGold();
        fullHealCost = (character.getMaxHP() - character.getHP()) / 5;
        fullHealText.text = "" + fullHealCost;
        if (fullHealCost > character.getGold()) {
            fullHealButton.enabled = false;
        } else {
            fullHealButton.enabled = true;
        }
        healthUpgradeText.text = "" + 5;
        if (5 > character.getGold()) {
            healthUpgradeButton.enabled = false;
        } else {
            healthUpgradeButton.enabled = true;
        }

        attackUpgradeText.text = "" + 10;
        if (10 > character.getGold()) {
            attackUpgradeButton.enabled = false;
        } else {
            attackUpgradeButton.enabled = true;
        }

        defenseUpgradeText.text = "" + 5;
        if (5 > character.getGold()) {
            defenseUpgradeButton.enabled = false;
        } else {
            defenseUpgradeButton.enabled = true;
        }

        moveUpgradeText.text = "" + 10;
        if (10 > character.getGold()) {
            moveUpgradeButton.enabled = false;
        } else {
            moveUpgradeButton.enabled = true;
        }
    }
}
