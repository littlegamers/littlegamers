﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Enemy")]
public class Enemy : ScriptableObject{

    public enum Location { Forest };

    public string enemyName;

    public int hp;

    public Sprite sprite;

    public int reward;

    public Location[] locations;

    public Move[] moves;
}

[System.Serializable]
public struct Move {
    public int physicalAttack;
    public int magicalAttack;
    public int armor;
    public int magicArmor;
    public int heal;
}
