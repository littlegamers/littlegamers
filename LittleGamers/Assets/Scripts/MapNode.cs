﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapNode : MonoBehaviour {

    private List<GameObject> connections;
    [SerializeField]
    private Sprite shopSprite = null;
    [SerializeField]
    private Sprite combatSprite = null;
    private Event node_event;
    private bool visited = false;

    // Start is called before the first frame update
    void Awake() {
        connections = new List<GameObject>();
    }

    public void SetNodeAttributes(int type, Enemy enemy) {
        node_event = new Event();
        node_event.type = type;
        node_event.enemy = enemy;

        Image image = GetComponent<Image>();

        switch (node_event.type) {
            case (0):
                image.sprite = combatSprite;
                break;
            case (2):
                image.sprite = shopSprite;
                break;
        }
    }

    public void ButtonClicked() {
        GetComponentInParent<MapController>().SelectNode(this);
    }

    public void NodeSelected() {
        Image image = GetComponent<Image>();
        image.color = Color.blue;
    }

    public void ClickOff() {
        if (!visited) {
            GetComponent<Image>().color = Color.white;
        }
    }

    public void NodeVisited() {
        visited = true;
        Image image = GetComponent<Image>();
        image.color = Color.black;
        GetComponent<Button>().enabled = false;
    }

    public void AddConnection(GameObject connection) {
        connections.Add(connection);
    }

    public void RemoveConnection(GameObject connection) {
        connections.Remove(connection);
    }

    public GameObject[] GetConnections() {
        return connections.ToArray();
    }

    public Event GetEvent() {
        return node_event;
    }

    public bool IsConnected(GameObject connection) {
        return connections.Contains(connection);
    }
}
