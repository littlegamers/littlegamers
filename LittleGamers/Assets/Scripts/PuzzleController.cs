﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PuzzleController : MonoBehaviour{

    [Header("UI Settings")]
    [SerializeField]
    private Text playerHPText = null;
    [SerializeField]
    private Text playerArmorText = null;
    [SerializeField]
    private Text playerMagicArmorText = null;
    [SerializeField]
    private Text enemyHPText = null;
    [SerializeField]
    private Text enemyArmorText = null;
    [SerializeField]
    private Text enemyMagicArmorText = null;
    [SerializeField]
    private Text movesLeftText = null;
    [SerializeField]
    private Text playerPhysicalAttackText = null;
    [SerializeField]
    private Text playerMagicalAttackText = null;
    [SerializeField]
    private Text enemyHealText = null;
    [SerializeField]
    private Text enemyPhysicalAttackText = null;
    [SerializeField]
    private Text enemyMagicalAttackText = null;
    [SerializeField]
    private Text enemyAddArmorText = null;
    [SerializeField]
    private Text enemyAddMagicArmorText = null;
    [SerializeField]
    private Image enemyImage = null;
    [SerializeField]
    private Image backgroundImage = null;
    [SerializeField]
    private Sprite[] backgroundImages = null;
    [SerializeField]
    private Animator VFXImage = null;
    [SerializeField]
    private GameObject WinPanel = null;
    [SerializeField]
    private GameObject LosePanel = null;
    [SerializeField]
    [Range(0.51f, 1f)]
    private float moveRatio = 0.85f;

    [Space(10)]

    [Header("Game Settings")]
    [SerializeField]
    private int runeGridHeight = 6;
    [SerializeField]
    private int runeGridWidth = 6;

    [Space(10)]

    [Header("Prefabs")]
    [SerializeField]
    private GameObject runePrefab = null;

    private Enemy enemy;
    private StatBlock playerStats;
    private StatBlock enemyStats;
    private GameObject[,] runeGrid;

    private int movesLeft;
    private int physicalDamage;
    private int magicDamage;
    private EnemyMove enemyMove;

    private int step = 0;
    private bool stepInProgress = false;
    private bool newRunesAdded = false;

    public void Update() {
        if (movesLeft == 0 && !stepInProgress) {
            if (step == 0) {
                StartCoroutine(MakeMatches(false));
                step++;
                stepInProgress = true;
            } else if (step == 1) {
                MakeRunesFall();
                newRunesAdded = AddNewRunes(true);
                step++;
                stepInProgress = true;
            } else if (step == 2) {
                ResetRunePositions();
                StartCoroutine(DealDamage());
                step++;
                stepInProgress = true;
            } else if (step == 3) {
                StartCoroutine(EnemyTurn());
                step++;
                stepInProgress = true;
            } else if (step == 4) {
                NewEnemyMove();
                step = 0;
                movesLeft = FindObjectOfType<Character>().getMoves();
            }
            UpdateUI();
        }else if (movesLeft == 0 && stepInProgress) {
            if (step == 2) {
                AnimateRunesFall();
            }
        }
    }

    public void StartPuzzle(Event puzzle_event) {

        FindObjectOfType<SoundManager>().PlayMusic("battle");

        enemy = puzzle_event.enemy;

        enemyImage.sprite = enemy.sprite;

        backgroundImage.sprite = backgroundImages[Random.Range(0, backgroundImages.Length)];

        playerStats = new StatBlock(40, 0, 0, playerHPText, playerArmorText, playerMagicArmorText);
        enemyStats = new StatBlock(25, 0, 0, enemyHPText, enemyArmorText, enemyMagicArmorText);

        movesLeft = FindObjectOfType<Character>().getMoves();

        physicalDamage = 0;
        magicDamage = 0;

        NewEnemyMove();

        runeGrid = new GameObject[runeGridHeight, runeGridWidth];

        for (int i = 0; i < runeGridHeight; i++) {
            for (int j = 0; j < runeGridWidth; j++) {
                runeGrid[i, j] = Instantiate(runePrefab, GetRunePosition(j, i), Quaternion.identity, this.transform);
                runeGrid[i, j].GetComponent<PuzzleRune>().SetRuneAttributes(j, i, (PuzzleRune.Type)Random.Range(0, 4), this);
            }
        }

        StartCoroutine(MakeMatches(true));
        MakeRunesFall();
        for (int i = 0; i < 1000; i++) {
            if (!AddNewRunes(false)) {
                break;
            }
            StartCoroutine(MakeMatches(true));
            MakeRunesFall();
        }

        ResetRunePositions();

        playerStats = new StatBlock(FindObjectOfType<Character>().getHP(), 0, 0, playerHPText, playerArmorText, playerMagicArmorText);
        enemyStats = new StatBlock(enemy.hp, 0, 0, enemyHPText, enemyArmorText, enemyMagicArmorText);

        physicalDamage = 0;
        magicDamage = 0;
        step = 0;

        UpdateUI();
    }

    public void ResetRunePositions() {
        for (int i = 0; i < runeGridHeight; i++) {
            for (int j = 0; j < runeGridWidth; j++) {
                if (runeGrid[i, j]) {
                    runeGrid[i, j].transform.position = GetRunePosition(j, i);
                }
            }
        }
    }

    public Vector2 PotentialMove(int x, int y, Vector2 directionRaw) {
        Vector2 swapPosition = CheckMove(x, y, directionRaw);

        int swapX = (int)swapPosition.x;
        int swapY = (int)swapPosition.y;

        if ((swapX == x && swapY == y) || movesLeft == 0) {
            ResetRunePositions();
        } else {
            runeGrid[y, x].transform.position = GetRunePosition(swapX, swapY);
            runeGrid[swapY, swapX].transform.position = GetRunePosition(x, y);
        }

        return swapPosition;
    }

    public void MakeMove(int x, int y, Vector2 swapPosition) {

        if (movesLeft > 0) {
            int swapX = (int)swapPosition.x;
            int swapY = (int)swapPosition.y;

            if (!(swapX == x && swapY == y)) {
                GameObject temp = runeGrid[y, x];
                runeGrid[y, x] = runeGrid[swapY, swapX];
                runeGrid[y, x].GetComponent<PuzzleRune>().SetXY(x, y);
                runeGrid[swapY, swapX] = temp;
                runeGrid[swapY, swapX].GetComponent<PuzzleRune>().SetXY(swapX, swapY);
                movesLeft--;
            }
        }
        UpdateUI();
    }

    private Vector3 GetRunePosition(int x, int y) {
        RectTransform rectTransform = this.GetComponent<RectTransform>();
        float runeLength = rectTransform.rect.width / runeGridWidth;
        Vector2 screenScaler = new Vector2(Screen.width / 1080f, Screen.height / 1920f);
        Vector2 screenSpacePos = new Vector2(this.transform.position.x + x * runeLength * screenScaler.x, this.transform.position.y + y * runeLength * screenScaler.y);
        return screenSpacePos;
    }

    private Vector2 CheckMove(int x, int y, Vector2 directionRaw) {
        Vector2 swapPosition = new Vector2(x, y);
        if (directionRaw.magnitude >= this.GetComponent<RectTransform>().rect.width/runeGridWidth/2) {

            Vector2 direction = directionRaw.normalized;

            if (Mathf.Abs(direction.x / 1) >= moveRatio || Mathf.Abs(direction.y / 1) >= moveRatio) {

                Vector2 directionOfMove;

                if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)) {
                    if (Mathf.Sign(direction.x) == 1) {
                        directionOfMove = new Vector2(-1, 0);
                    } else {
                        directionOfMove = new Vector2(1, 0);
                    }
                } else {
                    if (Mathf.Sign(direction.y) == 1) {
                        directionOfMove = new Vector2(0, -1);
                    } else {
                        directionOfMove = new Vector2(0, 1);
                    }
                }

                if (((directionOfMove.x >= 0 || x != runeGridWidth - 1) &&
                    (directionOfMove.x <= 0 || x != 0) &&
                    (directionOfMove.y >= 0 || y != runeGridHeight - 1) &&
                    (directionOfMove.y <= 0 || y != 0))) {

                    float swapX = x + directionOfMove.x * -1;
                    float swapY = y + directionOfMove.y * -1;

                    swapPosition = new Vector2(swapX, swapY);
                }
            }

        }
        return swapPosition;
    }

    private IEnumerator MakeMatches(bool instant) {

        bool[,] matchedGrid = new bool[runeGridHeight, runeGridWidth];

        for (int i = 0; i < runeGridHeight; i++) {
            for (int j = 0; j < runeGridWidth; j++) {
                matchedGrid[i, j] = false;
            }
        }

        for (int i = 0; i < runeGridWidth; i++) {
            PuzzleRune.Type currentType = runeGrid[0, i].GetComponent<PuzzleRune>().GetRuneType();
            int numberInMatch = 0;

            for (int j = 0; j < runeGridHeight; j++) {
                PuzzleRune rune = runeGrid[j, i].GetComponent<PuzzleRune>();
                if (rune.GetRuneType() == currentType) {
                    numberInMatch++;
                    if (numberInMatch == 3) {
                        matchedGrid[j, i] = true;
                        matchedGrid[j - 1, i] = true;
                        matchedGrid[j - 2, i] = true;
                    } else if(numberInMatch > 3) {
                        matchedGrid[j, i] = true;
                    }
                } else {
                    currentType = rune.GetRuneType();
                    numberInMatch = 1;
                }
            }
        }

        for (int i = 0; i < runeGridHeight; i++) {
            PuzzleRune.Type currentType = runeGrid[i, 0].GetComponent<PuzzleRune>().GetRuneType();
            int numberInMatch = 0;

            for (int j = 0; j < runeGridWidth; j++) {
                PuzzleRune rune = runeGrid[i, j].GetComponent<PuzzleRune>();
                if (rune.GetRuneType() == currentType) {
                    numberInMatch++;
                    if (numberInMatch == 3) {
                        matchedGrid[i, j] = true;
                        matchedGrid[i, j - 1] = true;
                        matchedGrid[i, j - 2] = true;
                    } else if (numberInMatch > 3) {
                        matchedGrid[i, j] = true;
                    }
                } else {
                    currentType = rune.GetRuneType();
                    numberInMatch = 1;
                }
            }
        }

        bool matchMade = false;

        for (int i = 0; i < runeGridHeight; i++) {
            for (int j = 0; j < runeGridWidth; j++) {
                if (matchedGrid[i, j]) {
                    matchMade = true;
                    PuzzleRune rune = runeGrid[i, j].GetComponent<PuzzleRune>();
                    runeGrid[i, j].GetComponent<Animator>().SetBool("Explode", true);
                    switch (rune.GetRuneType()) {
                        case PuzzleRune.Type.Sword:
                            physicalDamage += FindObjectOfType<Character>().getAttackMultiplier();
                            break;
                        case PuzzleRune.Type.Wand:
                            magicDamage += FindObjectOfType<Character>().getAttackMultiplier();
                            break;
                        case PuzzleRune.Type.Shield:
                            playerStats.armor += FindObjectOfType<Character>().getShieldMultiplier();
                            break;
                        case PuzzleRune.Type.MShield:
                            playerStats.magicArmor += FindObjectOfType<Character>().getShieldMultiplier();
                            break;
                        case PuzzleRune.Type.Heal:
                            playerStats.hp++;
                            break;
                    }
                }
            }
        }

        if (!instant) {
            if (matchMade) {
                FindObjectOfType<SoundManager>().PlaySoundEffect("match");
            }
            yield return new WaitForSeconds(0.42f);
        }

        for (int i = 0; i < runeGridHeight; i++) {
            for (int j = 0; j < runeGridWidth; j++) {
                if (matchedGrid[i, j]) {
                    Destroy(runeGrid[i, j]);
                    runeGrid[i, j] = null;
                }
            }
        }

        stepInProgress = false;
    }

    private void MakeRunesFall() {
        for (int w = 0; w < 1000; w++) {
            bool done = true;
            for (int i = 0; i < runeGridWidth; i++) {
                for (int j = runeGridHeight-1; j > 0; j--) {
                    if (runeGrid[j, i] && !runeGrid[j - 1, i]) {
                        done = false;
                        runeGrid[j - 1, i] = runeGrid[j, i];
                        runeGrid[j - 1, i].GetComponent<PuzzleRune>().SetXY(i, j - 1);
                        runeGrid[j, i] = null;
                    }
                }
            }
            if (done) {
                break;
            }
        }
    }

    private void AnimateRunesFall() {

        Vector2 screenScaler = new Vector2(Screen.width / 1080f, Screen.height / 1920f);

        bool done = true;
        for (int i = 0; i < runeGridWidth; i++) {
            for (int j = runeGridHeight-1; j > -1; j--) {
                if (runeGrid[j, i]) {
                    Vector2 runePos = runeGrid[j, i].transform.position;
                    Vector2 runeRestPos = GetRunePosition(i, j);
                    if (!Mathf.Approximately(runePos.y, runeRestPos.y)) {
                        done = false;
                        runeGrid[j, i].transform.Translate(0, -2000 * Time.deltaTime, 0);
                        runeGrid[j, i].transform.position = new Vector2(runeGrid[j, i].transform.position.x, Mathf.Max(runeRestPos.y, runeGrid[j, i].transform.position.y));
                    }
                }
            }
        }

        if (done) {
            stepInProgress = false;
            if (newRunesAdded) {
                step = 0;
            }
        }
    }

    private bool AddNewRunes(bool high) {
        bool newRunesAdded = false;
        float height = 0;
        if (high) {
            height = 1000;
        }
        for (int i = 0; i < runeGridHeight; i++) {
            for (int j = 0; j < runeGridWidth; j++) {
                if (!runeGrid[i, j]) {
                    newRunesAdded = true;
                    runeGrid[i, j] = Instantiate(runePrefab, GetRunePosition(j, i), Quaternion.identity, this.transform);
                    runeGrid[i, j].GetComponent<PuzzleRune>().SetRuneAttributes(j, i, (PuzzleRune.Type)Random.Range(0, 4), this);
                    runeGrid[i, j].transform.Translate(0, height, 0);
                }
            }
        }
        return newRunesAdded;
    }

    private IEnumerator DealDamage() {
        yield return new WaitForSeconds(0.5f);

        if (physicalDamage > 0) {
            enemyStats.ApplyDamage(physicalDamage, 0);
            VFXImage.SetTrigger("PhysicalAttack");
            FindObjectOfType<SoundManager>().PlaySoundEffect("playerPhysicalAttack");
            FindObjectOfType<SoundManager>().PlaySoundEffect(enemy.enemyName+"Hurt");
            physicalDamage = 0;
            enemyImage.transform.Translate(25, 0, 0);
            UpdateUI();
            yield return new WaitForSeconds(0.75f);
            enemyImage.transform.Translate(-25, 0, 0);
            yield return new WaitForSeconds(0.75f);
        }

        if (enemyStats.hp == 0) {
            FindObjectOfType<SoundManager>().PlaySoundEffect(enemy.enemyName + "Dead");
            yield return new WaitForSeconds(0.5f);
            WinPanel.SetActive(true);
            FindObjectOfType<SoundManager>().PlaySoundEffect("victory");
            yield return new WaitForSeconds(3.5f);
            stepInProgress = false;
            EndCombat(0);
        }

        if (magicDamage > 0) {
            enemyStats.ApplyDamage(0, magicDamage);
            VFXImage.SetTrigger("MagicalAttack");
            FindObjectOfType<SoundManager>().PlaySoundEffect("playerMagicAttack");
            FindObjectOfType<SoundManager>().PlaySoundEffect(enemy.enemyName + "Hurt");
            magicDamage = 0;
            enemyImage.transform.Translate(25, 0, 0);
            UpdateUI();
            yield return new WaitForSeconds(0.75f);
            enemyImage.transform.Translate(-25, 0, 0);
            yield return new WaitForSeconds(0.75f);
        }

        stepInProgress = false;

        if (enemyStats.hp == 0) {
            FindObjectOfType<SoundManager>().PlaySoundEffect(enemy.enemyName + "Dead");
            yield return new WaitForSeconds(0.5f);
            WinPanel.SetActive(true);
            FindObjectOfType<SoundManager>().PlaySoundEffect("victory");
            yield return new WaitForSeconds(3.5f);
            EndCombat(0);
        }
    }

    private IEnumerator EnemyTurn() {

        yield return new WaitForSeconds(0.5f);


        if (enemyMove.physicalDamage > 0) {
            playerStats.ApplyDamage(enemyMove.physicalDamage, 0);

            FindObjectOfType<SoundManager>().PlaySoundEffect("enemyPhysicalAttack");
            FindObjectOfType<SoundManager>().PlaySoundEffect("playerHurt");

            enemyImage.transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
            enemyMove.physicalDamage = 0;
            UpdateUI();

            yield return new WaitForSeconds(0.75f);
            enemyImage.transform.localScale = new Vector3(1f, 1f, 1f);
            yield return new WaitForSeconds(0.75f);
        }

        if (playerStats.hp == 0) {
            LosePanel.SetActive(true);
            FindObjectOfType<SoundManager>().PlaySoundEffect("failure");
            yield return new WaitForSeconds(3.5f);
            stepInProgress = false;
            EndCombat(1);
        }

        if (enemyMove.magicDamage > 0) {
            playerStats.ApplyDamage(0, enemyMove.magicDamage);

            FindObjectOfType<SoundManager>().PlaySoundEffect("enemyMagicAttack");
            FindObjectOfType<SoundManager>().PlaySoundEffect("playerHurt");

            enemyImage.transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
            enemyMove.magicDamage = 0;
            UpdateUI();

            yield return new WaitForSeconds(0.75f);
            enemyImage.transform.localScale = new Vector3(1f, 1f, 1f);
            yield return new WaitForSeconds(0.75f);
        }

        if (playerStats.hp == 0) {
            LosePanel.SetActive(true);
            FindObjectOfType<SoundManager>().PlaySoundEffect("failure");
            yield return new WaitForSeconds(3.5f);
            stepInProgress = false;
            EndCombat(1);
        }

        if (enemyMove.addArmor > 0) {
            enemyStats.armor += enemyMove.addArmor;

            FindObjectOfType<SoundManager>().PlaySoundEffect("enemyArmor");

            VFXImage.SetTrigger("ArmorUp");
            VFXImage.gameObject.GetComponent<Image>().color = Color.green;
            enemyMove.addArmor = 0;
            UpdateUI();

            yield return new WaitForSeconds(0.75f);
            VFXImage.gameObject.GetComponent<Image>().color = Color.white;
            yield return new WaitForSeconds(0.75f);
        }

        if (enemyMove.addMagicArmor > 0) {
            enemyStats.magicArmor += enemyMove.addMagicArmor;

            FindObjectOfType<SoundManager>().PlaySoundEffect("enemyMArmor");

            VFXImage.SetTrigger("ArmorUp");
            enemyMove.addMagicArmor = 0;
            UpdateUI();

            yield return new WaitForSeconds(0.75f);
            yield return new WaitForSeconds(0.75f);
        }

        if (enemyMove.healHP > 0) {
            enemyStats.hp += enemyMove.healHP;

            FindObjectOfType<SoundManager>().PlaySoundEffect("enemyHeal");

            enemyImage.color = new Color32(214, 75, 199, 255);
            enemyMove.healHP = 0;
            UpdateUI();

            yield return new WaitForSeconds(0.75f);
            enemyImage.color = Color.white;
            yield return new WaitForSeconds(0.75f);
        }


        yield return new WaitForSeconds(0.75f);
        stepInProgress = false;
    }

    private void NewEnemyMove() {
        int index = Random.Range(0, enemy.moves.Length);
        Move move = enemy.moves[index];
        enemyMove = new EnemyMove(move.heal, move.armor, move.magicArmor, move.physicalAttack, move.magicalAttack);
    }

    private void EndCombat(int winner) {
        if (winner == 0) {
            WinPanel.SetActive(false);
            movesLeft = 0;
            step = 10;
            Character character = FindObjectOfType<Character>();
            character.setHP(playerStats.hp);
            character.setGold(character.getGold() + enemy.reward);
            destroyPuzzle();
            FindObjectOfType<EventManager>().FinishPuzzle();
        } else {
            SceneManager.LoadScene(0);
        }
    }

    private void UpdateUI() {
        playerStats.UpdateUI();
        enemyStats.UpdateUI();
        enemyMove.UpdateUI(enemyHealText, enemyPhysicalAttackText, enemyMagicalAttackText, enemyAddArmorText, enemyAddMagicArmorText);

        movesLeftText.text = "Moves: " + movesLeft;

        playerPhysicalAttackText.text = "" + physicalDamage;
        playerMagicalAttackText.text = "" + magicDamage;
    }

    private void destroyPuzzle() {
        for (int i = 0; i < runeGridHeight; i++) {
            for (int j = 0; j < runeGridWidth; j++) {
                Destroy(runeGrid[i, j]);
            }
        }
    }
}

class StatBlock {
    public int hp { get; set; }
    public int armor { get; set; }
    public int magicArmor { get; set; }
    Text hpText;
    Text armorText;
    Text magicArmorText;

    public StatBlock(int hp, int armor, int magicArmor, Text hpText, Text armorText, Text magicArmorText) {
        this.hp = hp;
        this.armor = armor;
        this.magicArmor = magicArmor;
        this.hpText = hpText;
        this.armorText = armorText;
        this.magicArmorText = magicArmorText;
    }

    public void ApplyDamage(int physicalDamage, int magicDamage) {

        if (physicalDamage >= armor) {
            physicalDamage -= armor;
            armor = 0;
        } else {
            armor -= physicalDamage;
            physicalDamage = 0;
        }

        if (magicDamage >= magicArmor) {
            magicDamage -= magicArmor;
            magicArmor = 0;
        } else {
            magicArmor -= magicDamage;
            magicDamage = 0;
        }

        hp -= physicalDamage + magicDamage;

        if (hp < 0) {
            hp = 0;
        }
    }

    public void UpdateUI() {
        hpText.text = "" + hp;
        armorText.text = "" + armor;
        magicArmorText.text = "" + magicArmor;
    }
}

class EnemyMove{

    public int healHP { get; set; }
    public int addArmor { get; set; }
    public int addMagicArmor { get; set; }
    public int physicalDamage { get; set; }
    public int magicDamage { get; set; }

    public EnemyMove(int healHP, int addArmor, int addMagicArmor, int physicalDamage, int magicDamage) {
        this.healHP = healHP;
        this.addArmor = addArmor;
        this.addMagicArmor = addMagicArmor;
        this.physicalDamage = physicalDamage;
        this.magicDamage = magicDamage;
    }

    public void UpdateUI(Text enemyHealText, Text enemyPhysicalAttackText, Text enemyMagicalAttackText, Text enemyArmorText, Text enemyMagicShieldText) {

        enemyHealText.text = "" + healHP;
        enemyPhysicalAttackText.text = "" + physicalDamage;
        enemyMagicalAttackText.text = "" + magicDamage;
        enemyArmorText.text = "" + addArmor;
        enemyMagicShieldText.text = "" + addMagicArmor;
    }
}