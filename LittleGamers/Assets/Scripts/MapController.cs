﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MapController : MonoBehaviour {

    [Header("UI Settings")]
    [SerializeField]
    private Text HPText = null;
    [SerializeField]
    private Text CoinText = null;
    [SerializeField]
    private Button StartEventButton = null;

    [Space(10)]

    [SerializeField]
    private Vector2 perturb_amount = new Vector2(0.5f, 0.5f);

    [Space(10)]

    [Header("Game Settings")]
    [SerializeField]
    private int number_of_layers = 5;

    [Space(10)]

    [Header("Prefabs")]
    [SerializeField]
    private GameObject node_prefab = null;
    [SerializeField]
    private GameObject line_prefab = null;
    [SerializeField]
    private Enemy boss = null;
    [SerializeField]
    private Enemy[] enemies = null;

    private List<List<GameObject>> map_nodes;
    private MapNode selectedNode;
    private MapNode currentNode;

    // Start is called before the first frame update
    void Start() {

        FindObjectOfType<SoundManager>().PlayMusic("map");

        map_nodes = new List<List<GameObject>>();

        for (int i = 0; i < number_of_layers; i++) {
            map_nodes.Add(new List<GameObject>());
        }

        CreateNodes();
        PerturbNodes();
        ConnectNodes();
        CorrectOverlap();
        DrawLines();

        UpdateUI();
    }

    public void SelectNode(MapNode map_node) {
        UnselectNode();
        if (currentNode.IsConnected(map_node.gameObject)) {
            StartEventButton.gameObject.SetActive(true);
            map_node.NodeSelected();
            selectedNode = map_node;
        }
    }

    public void UnselectNode() {
        if (selectedNode) {
            StartEventButton.gameObject.SetActive(false);
            selectedNode.ClickOff();
            selectedNode = null;
        }
    }

    public void StartEvent() {
        if (selectedNode) {
            selectedNode.NodeVisited();
            currentNode = selectedNode;
            FindObjectOfType<EventManager>().StartEvent(selectedNode.GetEvent());
        }
    }

    public void UpdateMap() {
        FindObjectOfType<SoundManager>().PlayMusic("map");
        if (currentNode.gameObject == map_nodes[number_of_layers-1][0]) {
            SceneManager.LoadScene(0);
        }
        UnselectNode();
        UpdateUI();
    }

    public void UpdateUI() {
        Character character = FindObjectOfType<Character>();
        HPText.text = "" + character.getHP();
        CoinText.text = "" + character.getGold();

        for (int i = 0; i < number_of_layers; i++) {
            List<GameObject> layer = map_nodes[i];
            for (int j = 0; j < layer.Count; j++) {
                if (currentNode.IsConnected(layer[j])) {
                    layer[j].GetComponent<Button>().enabled = true;
                } else {
                    layer[j].GetComponent<Button>().enabled = false;
                }
            }
        }
    }

    private void CreateNodes() {
        RectTransform rectTransform = this.GetComponent<RectTransform>();
        Vector2 screenScaler = new Vector2(Screen.width / 1080f, Screen.height / 1920f);

        List<GameObject> firstLayer = map_nodes[0];
        GameObject firstNode = Instantiate(node_prefab, new Vector2(transform.position.x + rectTransform.rect.width / 2 * screenScaler.x, transform.position.y * screenScaler.y), Quaternion.identity, this.transform);
        currentNode = firstNode.GetComponent<MapNode>();
        currentNode.SetNodeAttributes(2, null);
        currentNode.NodeVisited();
        firstLayer.Add(firstNode);

        List<GameObject> lastLayer = map_nodes[number_of_layers - 1];
        GameObject lastNode = Instantiate(node_prefab, new Vector2(transform.position.x + rectTransform.rect.width / 2 * screenScaler.x, transform.position.y + rectTransform.rect.height * screenScaler.y), Quaternion.identity, this.transform);
        lastNode.GetComponent<MapNode>().SetNodeAttributes(0, boss);
        lastLayer.Add(lastNode);


        for (int i = 1; i < number_of_layers-1; i++) {
            List<GameObject> layer = map_nodes[i];

            int numberOfNodes = Random.Range(Mathf.Max(map_nodes[i-1].Count-2, 3), Mathf.Min(map_nodes[i - 1].Count+1, 5)+1);

            for (int j = 0; j < numberOfNodes; j++) { 
                GameObject node = Instantiate(node_prefab, new Vector2(transform.position.x + rectTransform.rect.width / (numberOfNodes - 1) * (j) * screenScaler.x, transform.position.y + rectTransform.rect.height / (number_of_layers-1) * (i) * screenScaler.y), Quaternion.identity, this.transform);
                int type = 0;
                if (Random.value < 0.135) {
                    type = 2;
                }
                node.GetComponent<MapNode>().SetNodeAttributes(type, enemies[Random.Range(0, enemies.Length)]);
                layer.Add(node);
            }
        }
    }

    private void ConnectNodes() {
        for (int i = 0; i < number_of_layers-1; i++) {
            List<GameObject> layer = map_nodes[i];
            List<GameObject> nextLayer = map_nodes[i + 1];
            if (i == 0) {
                for (int k = 0; k < nextLayer.Count; k++) {
                    layer[0].GetComponent<MapNode>().AddConnection(nextLayer[k]);
                }
            } else {
                for (int j = 0; j < layer.Count; j++) {
                    MapNode node = layer[j].GetComponent<MapNode>();
                    if (i == number_of_layers - 2) {
                        node.AddConnection(nextLayer[0]);
                    } else {
                        if (j < nextLayer.Count) {
                            node.AddConnection(nextLayer[j]);
                        } else {
                            node.AddConnection(nextLayer[nextLayer.Count-1]);
                        }
                        if (j > 0 && j - 1 < nextLayer.Count) {
                            node.AddConnection(nextLayer[j - 1]);
                        }
                        if (j + 1 < nextLayer.Count) {
                            node.AddConnection(nextLayer[j + 1]);
                        }
                    }
                }
            }
        }
    }

    private void CorrectOverlap() {
        for (int i = 1; i < number_of_layers - 2; i++) {
            List<GameObject> layer = map_nodes[i];
            List<GameObject> nextLayer = map_nodes[i+1];
            for (int j = 0; j < layer.Count-1; j++) {
                MapNode node = layer[j].GetComponent<MapNode>();
                MapNode nextNode = layer[j+1].GetComponent<MapNode>();
                for (int k = 1; k < nextLayer.Count; k++) {
                    if (node.IsConnected(nextLayer[k]) && nextNode.IsConnected(nextLayer[k - 1])) {
                        if (Random.value > 0.5f) {
                            node.RemoveConnection(nextLayer[k]);
                        } else {
                            nextNode.RemoveConnection(nextLayer[k-1]);
                        }
                    }
                }
            }
        }
    }

    private void PerturbNodes() {
        RectTransform rectTransform = this.GetComponent<RectTransform>();
        Vector2 position = this.transform.position;
        for (int i = 1; i < number_of_layers - 1; i++) {
            List<GameObject> layer = map_nodes[i];
            for (int j = 0; j < layer.Count; j++) {
                layer[j].transform.position = new Vector2(Mathf.Clamp(layer[j].transform.position.x + Random.Range(-perturb_amount.x, perturb_amount.x), position.x, position.x + rectTransform.rect.width), Mathf.Clamp(layer[j].transform.position.y + Random.Range(-perturb_amount.y, perturb_amount.y), position.y, position.y + rectTransform.rect.height));
            }
        }
    }

    private void DrawLines() {
        Vector2 screenScaler = new Vector2(Screen.width / 1080f, Screen.height / 1920f);

        for (int i = 0; i < number_of_layers - 1; i++) {
            List<GameObject> layer = map_nodes[i];
            for (int j = 0; j < layer.Count; j++) {
                MapNode node = layer[j].GetComponent<MapNode>();
                GameObject[] connections = node.GetConnections();
                for (int k = 0; k < connections.Length; k++) {
                    float angle = -Mathf.Atan((layer[j].transform.position.x - connections[k].transform.position.x) / (layer[j].transform.position.y - connections[k].transform.position.y)) * 180 / Mathf.PI;
                    GameObject line = Instantiate(line_prefab, new Vector2(layer[j].transform.position.x, layer[j].transform.position.y), Quaternion.Euler(0, 0, angle), this.transform);
                    line.GetComponent<RectTransform>().sizeDelta = new Vector2(30, Vector2.Distance(layer[j].transform.position, connections[k].transform.position) * 1/screenScaler.y);
                    line.GetComponent<RectTransform>().SetAsFirstSibling();
                }
            }
        }
    }
}
