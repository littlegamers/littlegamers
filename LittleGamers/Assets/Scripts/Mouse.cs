﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEditor;

public class Mouse : MonoBehaviour
{
    public GameObject quillPen;
    public GameObject quillPen2;
    public void PointerEnter()
    {
        transform.localScale = new Vector2(1.5f, 1.5f);
        print("MouseEntered");
        quillPen.SetActive(true);
        quillPen2.SetActive(false);
    }

    public void PointerExit()
    {
        transform.localScale = new Vector2(1f, 1f);
        print("MouseExited");
        quillPen.SetActive(false);
        quillPen2.SetActive(true);
    }

    public void PointerEnterBack()
    {
        transform.localScale = new Vector2(1.4f, 1.4f);
        print("MouseEntered");
    }

    public void PointerExitBack()
    {
        transform.localScale = new Vector2(1.1f, 1.1f);
        print("MouseExited");
    }
}
